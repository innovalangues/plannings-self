const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const requestIp = require('request-ip');
const compression = require('compression');
const helmet = require('helmet');


app.use(express.json());
app.use(express.static(__dirname + '/public'));
//app.use('/static', express.static(__dirname + '/public'));
app.use(compression()); // Compress all routes
app.use(helmet()); // Protection de base pour l'appli

// DÉCLARATION DES CHEMINS
const FilebaseDeDonnees = '../db/db_plannings-self.json';


// LECTURE DE LA BASE DE DONNÉES
var data = fs.readFileSync(FilebaseDeDonnees);
var baseDeDonnees = JSON.parse(data);
console.log('La base de données contient '+Object.keys(baseDeDonnees).length+' entrées.');


///////////////////////////////////////
//////// CHARGEMENT PAGE VIERGE ///////
///////////////////////////////////////
app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

///////////////////////////////////////
///// SAUVEGARDE DE LA PAGE CLIENT ////
///////////////////////////////////////
app.post('/phonodrop/export', (request,response) => {
    var colis = request.body;
    colis.ipCli = requestIp.getClientIp(request);
    baseDeDonnees[colis.pageId] = colis;
    response.json({
        status: "success",
        identifiant:colis.pageId
    });
    fs.writeFile(FilebaseDeDonnees, JSON.stringify(baseDeDonnees, null, 2), finished);
    function finished(err) {
        console.log("Colis reçu depuis "+requestIp.getClientIp(request));
    }
    console.log('La base de données contient maintenant '+Object.keys(baseDeDonnees).length+' entrées.');
})

// PORT
const {port=8080} = process.env;
app.listen(port, () => console.log(`Listening on port ${port}...`));

// server.listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
//   });

